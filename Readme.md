# GEK 92T Systemintegration "Storage Virtualization"

## Kacper Urbaniec | 5AHIT | 12.12.2019

# Aufgabenstellung

Fassen Sie die Aufgaben und Vorteile der Storage Virtualisierung zusammen. Vergleichen Sie, wie eine derartige Virtualisierung von den gebräuchlichsten Umgebungen (Microsoft, VMware, Linux z.B. am Beispiel Red Hat, ...) realisiert wird, welche Features diese bieten und welche Voraussetzungen diese benötigen.

## Quellenvorschläge

- [Storage Virtualization Workbook](http://cdn.ttgtmedia.com/searchStorage/downloads/StorageVirt_ezine080108REV3.pdf)
- [Storage Virtualization I - What, Why, Where and How?](http://www.snia.org/sites/default/education/tutorials/2011/spring/virtualization/PeglarRob-Virtualization_I.pdf)
- [VMware® vSphere Virtual Volumes (vVols): Getting Started Guide](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/products/virtualvolumes/vmw-vsphere-virtual-volumes-vvolvs-getting-started-guide.pdf)
- [Red Hat Enterprise Linux 6 - Logical Volume Manager Administration](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/pdf/logical_volume_manager_administration/Red_Hat_Enterprise_Linux-6-Logical_Volume_Manager_Administration-en-US.pdf)

## Abgaberichtlinien

- Ausarbeitung als PDF mit Quellenangabe
- min. 1 Seite A4 Netto (mit "normaler" Schriftgröße, ohne Bilder), max. 5 Seiten A4 Brutto (mit Bildern)

## Beurteilungskriterien

### Grundlegend

- Fassen Sie die Aufgaben und Vorteile der Storage Virtualisierung zusammen.
- Beschreiben Sie eine konkrete Implementierung (Features, Voraussetzungen, Administration, Performance, ...)
- Vergleichen Sie Storage-Virtualisierung mit Verteilten Dateisystemen (GlusterFS, OCFS2, ...)

### Erweitert

- Skizzieren Sie eine geeignete Systemarchitektur (insbesondere in Hinblick auf Storage) für das TGM (Anmerkung: Server-Virtualisierung, Desktop-Virtualisierung)
- Vergleichen Sie mindestens 3 konkrete Implementierungen (Features, Voraussetzungen, Administration, Performance, ...)

# Implementierung

## Wieso Storage Virtualisierung?

* Vereinfachung der Verwaltung

  Heutzutage  gibt es immer mehr heterogene System

  * Verschiedene Server - physikalisch auch als virtuell
  * Verschiedene Betriebssysteme
  * Verschiedene Speichersysteme und Protokolle

  * Verschiedene Management Konsolen

* Sicherstellen von Verfügbarkeit

  Immer mehr Services bieten 24/7 Dienste, dabei muss Hochverfügbarkeit garantiert werden, auch beim Speicher.

* Bessere Speicherauslastung

  Mit traditionalen Speicher hat man oft das Problem, dass manche Festplatte wenig utilisiert werden, während andere fast am Speichermaximum arbeiten.

Storage Virtualisierung kann diese Probleme lösen und hilft die Komplexität der Architektur  zu vereinfachen.

## Was ist Storage Virtualisierung genau?

Storage Virtualisierung ist das Zusammenführen von physischem Speicher von mehreren Speichermedien zu einem scheinbar einzigen Speichergerät - oder einem Pool verfügbarer Speicherkapazität -, das von einer zentralen Konsole aus verwaltet wird. Dadurch erscheint der Verbund der Speichermedien einem Client als eine logische Einheit.

Somit wird eine Transparenz in den Bereichen der Lokalität und Implementierung sichergestellt.

Weiters erlaubt Storage Virtualisierung dynamische Operationen, die auf normalen Speichermedien nicht möglich wären. Somit kann man "on the fly" den Speicher konfigurieren.

Weiters ist es zu beachten, dass es nicht die "Eine" Storage Virtualisierung gibt, es gibt verschiedene Umsetzungsmöglichkeiten, die sich für einige Fälle besser eignen als andere.

## Vorteile der Storage Virtualisierung

* Verbesserte Speicherauslastung

  * Dadurch niedrigere Anforderungen in Bezug auf Energie, Kühlung ,und Platz

* Offen für Änderungen in Bezug auf neue Server, Netzwerk und Speicher-Technologien

* Signifikant reduzierte Downtime - sei sie geplant oder ungeplant

* Architektur der Zukunft

  * Erhöhte Stabilität, Sicherheit und Flexibilität
  * Verwaltung der File Systeme und Volume Managers

  

## Arten von Storage Virtualisierung

### Block Virtualization

Virtualize LUNs presented to applications

Virtualisiert LUNs (Logical Unit Number, logische Einheit einer physischen Festplatte), die Applikationen oder Clients angeboten werden.

### Disk Virtualization

Speicher mehrerer physischer Medien wird in Chunks runter gebrochen und zusammen in einen Storage-Pool (Aggregierte Kapazität aus unterschiedlichen Speicher-Ressourcen) vereint, der benutzt wird um LUNs zu kreieren. 

### Tape Virtualization

Erzeugt eine virtuelles Tape, einen Speicher auf Basis eines Disk Arrays.

### File Virtualization

Virtualisiert NAS und File Serve zu einen einzigen Namespace.

## Vergleich GlusterFS - OCFS2

### GlusterFS

* Cluster-Nodes bilden eine Client-Server-Architektur über TPC/IP

* Unterstützte Betriebsarten:

  * Standalone Storage

    Ein einzelner Server, der das Dateisystem über das Netzwerk bereitstellt 

  - Distributed Storage

    Mehrere Server, die die Daten untereinander verteilt speichern und diese den Clients bereitstellen

  - Replicated Storage

    Mehrere Server, die die Daten untereinander gespiegelt speichern und diese den Clients bereitstellen

  - Distributed Replicated Storage

    Mehrere Server, die die Daten untereinander verteilt und gespiegelt speichern

  - Striped Storage

    Mehrere Server, bei welchen die Daten gestriped werden, um eine höhere Performance und Disk-IO-Bandbreite zu liefern

  - Cloud/HPC Storage

    Siehe Distributed Replicated Storage

  - NFS-like Standalone Storage Server-2

    ähnlich Standalone Storage, es wird mehr als nur ein Dateisystem bereitgestellt

  - Aggregating Three Storage Servers with Unify

    Drei Server, die ein einheitliches Dateisystem mittels Unify bereitstellen, ohne Redundanz

### OCFS2

* Benutzt iSCSI Speicher
* Bestandteil des Linux-Kernels
* Koordination der Nodes erfolgt über Lock Manager
* Unterstützung von heterogenen Cluster mit unterschiedlichen Node-Architekturen (x86, x86_64)

## Systemarchitektur

Virtualisierte Server mittels VMware.

Anbindung des Shared Storages mittels VMware VMware ESXi.

Siehe https://theitbros.com/share-disk-between-vms-on-vmware-esxi/.

## Quellen

* [Storage Virtualization Workbook](http://cdn.ttgtmedia.com/searchStorage/downloads/StorageVirt_ezine080108REV3.pdf)
* [Storage Virtualization I - What, Why, Where and How?](http://www.snia.org/sites/default/education/tutorials/2011/spring/virtualization/PeglarRob-Virtualization_I.pdf)
* [What is Storage Virtualization](https://searchstorage.techtarget.com/definition/storage-virtualization)
* [Storage Pools](https://www.computerweekly.com/de/definition/Storage-Pools)
* [GlusterFS](https://docs.gluster.org/en/latest/Quick-Start-Guide/Architecture/)
* [GlusterFS Betriebsarten](https://de.wikipedia.org/wiki/GlusterFS)
* [OCFS2](https://blogs.oracle.com/cloud-infrastructure/a-simple-guide-to-oracle-cluster-file-system-ocfs2-using-iscsi-on-oracle-cloud-infrastructure)
* [VMWare ESXI](https://theitbros.com/share-disk-between-vms-on-vmware-esxi/)